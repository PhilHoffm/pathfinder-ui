# FoundryVTT - Pathfinder-ui v3
A dodgy fork of RPG Styled Ui by Xbozon for Pathfinder 2e.

## Installation

To install, follow these instructions:

1.  Inside Foundry, select the Game Modules tab in the Configuration and Setup menu.
2.  Click the Install Module button and enter the following URL: 
https://gitlab.com/sasmira/pathfinder-ui/-/raw/master/module/module.json
3.  Click Install and wait for installation to complete.

## Pathfinder Ui v3, it's :

1.  __**New Theme based on RPG Styled UI Foundry VTT by [Xbozon](https://github.com/Xbozon/rpg-styled-ui)**__
![Pathfinder-ui](pathfinder-ui.png)
2.  __**Dicefinder**__, New dice for Pathfinder ! require [Dice-So-Nice](https://gitlab.com/riccisi/foundryvtt-dice-so-nice)
![Dicefinder](dicefinder.png)
3.  __**Combat Carousel Compatibility**__  ! require [Combat Carousel](https://github.com/death-save/combat-carousel)
![combat-carousel](combat-carousel.png)
4.  __**Translation**__, French and English !


## Version & Compatibility

- Tested on v9.255 version.
